var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var json_start ="{";
    var json_end="}";
    var json = "";
    db.all('SELECT * FROM USERS', function(err, rows) {
        

        // create json  from db rows
        json +=json_start;
        json +="\"users\":["

      
        //create json array element
        for (i=0; i< rows.length;i++){
          json +=json_start;

          json += "\"id\":" + "\"" + rows[i].ID + "\"";
          json +=","
          json += "\"username\"" + ":" + "\"" + rows[i].USERNAME + "\"";
          json +=","
          json += "\"email\"" + ":" +  "\"" + rows[i].EMAIL  + "\"";
          json +=","
          json +="\"fullname\"" + ":" + "\"" + rows[i].FULLNAME + "\"";
          json +=","
          json += "\"age\"" + ":" + "\"" + rows[i].AGE + "\"";
          json +=","
          json += "\"location\"" + ":" + "\"" + rows[i].LOCATION + "\"";
          json +=","
          json +="\"gender\"" + ":" + "\"" + rows[i].GENDER + "\"";

          json +=json_end;
          //check if it's last element in array
          if (  i < rows.length -1){
            json +=","
          }
        }
        json +="]";
        json +=json_end;

        console.log("got json: " + json);
        res.json(JSON.parse(json));
    });
});


/**
{ username: 'Niko',
  email: 'nmay@lycos.com',
  fullname: 'Nikolaj Majorov',
  age: '10',
  location: 'GVA',
  gender: 'M' }
  **/


/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    console.log("add user");
    var db = req.db;

    var json = req.body;
    console.log(json);
    db.serialize(function() {
      var id = randomInt();
      var sql= "INSERT INTO USERS \
                              VALUES (" +
                                id +",\""+ json.username + "\", \"" +
                                json.email+ "\",\"" + json.fullname + "\",\"" +
                                json.age +"\",\"" + json.location + "\",\"" +
                                json.gender+"\")"
      console.log(sql);
      var stmt = db.prepare(sql);

      stmt.run(function(err){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
      });
      stmt.finalize();


    });


});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var userToDelete = req.params.id;
    console.log(userToDelete);
  db.run('DELETE FROM USERS WHERE ID = ?', userToDelete, function(err, row) {
           res.send((err === null) ? { msg: '' } : { msg:'error: ' + err });
        });
});

//get random integer for db id
function randomInt () {
    return Math.floor(Math.random() * (100000 - 1) + 1);
}

module.exports = router;
